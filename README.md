[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Práctica 1: Tic Tac Toe ( Tres en Raya )
Juan Pedro Fernández Torralbo. Curso 19/20
## Introducción

En esta práctica se realizarán partidas del juego [Tres En Raya](https://es.wikipedia.org/wiki/Tres_en_l%C3%ADnea) con varios agentes. Para ello se usarán 3 protocolos de JADE distintos. 

* Propose
* Request
* Subscribe

Dichos protocolos necesitarán de 2 roles: Iniciador y Participante. Estos roles se implementarán con JADE a través de la herencia de las clases:

| Protocolo | Clase para el Iniciador | Clase para el Participante |
|-----------|-------------------------| -------------------------- |
| FIPA-Request | [`AchieveREInitiator`](https://jade.tilab.com/doc/api/jade/proto/AchieveREInitiator.html) | [`AchieveREResponder`](https://jade.tilab.com/doc/api/jade/proto/AchieveREResponder.html) |
| FIPA-Propose | [`ProposeInitiator`](https://jade.tilab.com/doc/api/jade/proto/ProposeInitiator.html) | [`ProposeResponder`](https://jade.tilab.com/doc/api/jade/proto/ProposeInitiator.html) |
| FIPA-Subscribe | [`SubscriptionInitiator`](https://jade.tilab.com/doc/api/jade/proto/SubscriptionInitiator.html) | [`SubscriptionResponder`](https://jade.tilab.com/doc/api/jade/proto/SubscriptionResponder.html) |

En nuestro caso, tendremos estas tareas encargadas:

| Protocolo | Tarea para el Iniciador | Tarea para el Participante |
|-----------|-------------------------| -------------------------- |
| FIPA-Request |TareaRequestIniciaMovimiento | TareaRequestResponseMovimiento |
| FIPA-Propose | TareaProposeIniciaPartida | TareaProposeResponderPartida |
| FIPA-Subscribe | TareaIniciaSubscripcionPartida | TareaResponseSubscripcionPartida |

También , se usa la Tarea: _**TareaSubscripcionDF**_ , junto a la Interface _SubscripcionDF_ para subscribirse al Servicio de Paginas Amarillas . A continuación se explicarán las funciones de cada Tarea , pero primero debemos saber la logica detrás de estas clases:

## Estructura del Programa

La estructura del programa es la que he pensado yo para mi programa. Podrá no ser la más eficiente ni la que mejor controle recursos de JADE. El TTT por envio de mensajes se hará con la estructura:
* `AgenteTablero` controla las partidas entre `AgenteJugador`
* `AgenteTablero` solo puede controlar 3 _**Partida**_ y los `AgenteJugador` solo pueden jugar 3 _**Partida**_ simultáneamente , es decir, el máximo de _**Partida**_ será 3
* La partida transcurrirá mediante envío de _**Movimiento**_ . NUNCA de todo el juego
* Lo anterior nos lleva a que cada `AgenteJugador` tendrá un _**Tablero**_ propio, que actualizará con cada _**Movimiento**_ , tanto el suyo como el del rival
* `AgenteTablero` será por tanto, el encargado de comunicar a cada  `AgenteJugador`  los _**Movimiento**_ de cada _**Partida**_ .
 
 Esto, pasado a estructura de código será:

1. El `AgenteTablero` se subscribe a las Paginas Amarillas con la  `TareaSubscripcionDF` y encuentra a los `AgenteJugador` disponibles.
2. El `AgenteTablero` propone a los `AgenteJugador` una partida de TTT. Si estos aceptan, el `AgenteTablero` los guarda para su futuro uso
3. El `AgenteTablero` inicia un  _**Tablero**_ de TTT para cada 2 `AgenteJugador` , asignandole a cada uno un _**ID de Partida**_ . Los `AgenteJugador` pertenecientes a dicha partida se subscribirán al `AgenteTablero` para recibir el resultado de esta partida a su finalización.
4. Para cada  _**Tablero**_ , `AgenteTablero` solicitará a uno de los dos `AgenteJugador` un  _**Movimiento**_ , que el propio `AgenteTablero` controlará que se introduzca en el _**Tablero**_ . Una vez hecho esto, mandará este primer movimiento al otro `AgenteJugador` , a la vez que se le requiere el envio de su movimiento.
5. La _**Partida**_ transcurrirá a partir del envío contínuo de estos mensajes , hasta que el `AgenteTablero` encuentre un ganador para cada una. Si lo hace, la _**Partida**_ acabará y el `AgenteTablero` informará del resultado a ambos `AgenteJugador` para que puedan realizar otras partidas.

A continuación explicaremos cada Protocolo, junto a su logica

### 3.1.1 Protocolo FIPA-Propose

Para el inicio de las partidas primero deberemos conocer cuantos de los AgenteJugador estan interesados en jugar una partida . Para ello , usaremos el protocolo de **JADE**: **FIPA-Propose**

#### Estructura 
```mermaid
sequenceDiagram
	participant i as AgenteTablero 
	participant r as AgenteJugador
	i->>r: PROPOSE (Partida) 
	Note right of r: String que solicita  <br/>Partida
	
	alt no definida
		r->>i: REJECT_PROPOSAL (Respuesta)
	else completada
		r->>i: ACCEPT_PROPOSAL (Respuesta)
	end
    Note left of i: Respuesta a la <br/>solicitud
```
#### TareaProposeIniciaPartida

Esta Tarea será la encargada de crear las proposiciones (en su construccion) y de recibir las solicitudes de los `AgenteJugador` para que el `AgenteTablero` las trate. Si se ha recibido un _REJECT_PROPOSAL_ se ignorará a dicho agente para futuros tratamientos. Sin embargo, si este acepta, se introducirá en una lista de "parJugadores" . Para cada 2 elementos pertenecientes a esta lista, se creará una Partida, con su correspondiente tablero , y se creará un Request del primer movimiento a uno de ellos. Esto quiere decir que cada Request siempre irá enlazado a 2 y solo 2 `AgenteJugador` , por lo que se cumplirá  la lógica de mensajes entre varias Partidas. 
#### TareaProposeResponderPartida

Esta tarea corresponderá al `AgenteJugador` , de la misma forma que la Tarea anterior era gestionada por el `AgenteTablero` . Esta tarea recibe el mensaje de proposicion de Partida que se creó en la construccion del IniciaPartida . Si el jugador aun no está en 3 partidas, aceptará , con el correspondiente mensaje de respuesta. Si bien el agente ya está jugando en 3 Partidas diferentes  denegará la petición , con el correspondiente mensaje de respuesta 

#### Respuesta a la solicitud de la operación

 La respuesta que el  `AgenteJugador` da al `AgenteTablero` a su solicitud. Las opciones que se van ha considerar son las siguientes:

- `REJECT_PROPOSAL`: Solo se hará en el caso de participar en otras 3 partidas

- `ACCEPT_PROPOSAL`: El `AgenteJugador` aceptará jugar y el `AgenteTablero`  lo añadirá a _listaJugadores_



Se recorrerá toda la estructura de los mensajes y se procesarán uno a uno. Si llegan mensajes que no corresponden a estas dos estructuras, se tratarán en el metodo _handleOutOfSequence_


#### Tratar mensajes fuera de secuencia

Hay que tener en cuenta que alguno de los `AgenteJugador` no responda de forma apropiada según los mensajes esperados en el protocolo y para ello hay que implementar el método `handleOutOfSequence(.)` para evitar que la tarea no finalice de forma apropiada que pueda provocar que el `AgenteTablero` termine finalizando de forma  errónea 

### 3.1.1 Protocolo FIPA-Request

Una vez creadas las Partidas, deberemos realizar el intercambio de Movimientos de cada Partida. Para ello , usaremos el protocolo de **JADE**: **FIPA-Request**

#### Estructura 
```mermaid
sequenceDiagram
	participant i as AgenteTablero 
	participant r as AgenteJugador
	i->>r: REQUEST (Movimiento) 
	Note right of r: Petición de un <br/>Movimiento
	alt desconocido
		r-->>i: NOT_UNDERSTOOD (Respuesta)
    else rechazo
	    r-->>i: REFUSE (Respuesta)
	else conforme
		r-->>i: AGREE (Respuesta)
	end
	Note right of r: Recepción de<br/> Movimiento anterior  </br>y Resolución de nuevo<br/>Movimiento 
	alt no definida
		r->>i: FAILURE (Movimiento)
	else completada
		r->>i: INFORM (Movimiento)
	end
    Note left of i: Respuesta es<br/> un String con  </br> Información
```
### TareaRequestIniciaMovimiento
 
Esta será la Tarea que representa al `AgenteTablero` para mandar las solicitudes de _**Movimiento**_ a los `AgenteJugador` ( en su constructor ) y recibir las respuestas a estas solicitudes. Estas Respuestas se dividirán en 2 tipos:

* Respuestas: Como su nombre indica, son los mensajes de respuesta a la solicitud enviada.Pueden ser
  * `NOT_UNDERSTOOD`: En este ejemplo solo consideraremos esta opción si en el contenido del mensaje de la solicitud era nulo.
  * `REFUSE`: Si el Movimiento recibido es  vacio (id  de la Partida será nulo), se opta por no hacer nada.
  * `AGREE`: El `AgenteJugador` realizará el Movimiento y el `AgenteTablero` espera a su resultado.
* Movimiento: Mensaje enviado tras el `AGREE`. Puede ser:
  * `FAILURE`:  La respuesta será un Movimiento nulo, pues el `AgenteJugador` no pudo procesar bien el jugador 
  * `INFORM`: Si se puede realizar un Movimiento basado en el recibido . El resultado que recibe el `AgenteTablero` es un Movimiento . Dicho Movimiento será introducido en el Tablero correspondiente al _**idPartida**_ del propio Movimiento y se llamará a una función `compruebaVictoria()` que como su  nombre indica comprueba si es un movimiento ganador. En caso afirmativo , la Partida termina y se responde al subscribe que los `AgenteJugador` iniciaron en el Propose. En caso negativo, usará este Movimiento recibido para crear otro Request al otro miembro de la partida y que la Partida prosiga hasta que se decida un ganador o se termine en empate

### TareaRequestResponseMovimiento

Esta será la Tarea que representa al `AgenteJugador` para mandar las solicitudes de _**Movimiento**_ de los `AgenteTablero` a este y responder satisfactoriamente  a estas solicitudes. Estas Respuestas se dividirán en 2 tipos:

* Respuestas: Como su nombre indica, son los mensajes de respuesta a la solicitud enviada.Pueden ser
  * `NOT_UNDERSTOOD`: En este ejemplo solo consideraremos esta opción si en el contenido del mensaje de la solicitud era nulo.
  * `REFUSE`: El `AgenteJugador` rechaza la Partida. Esto será indicado teniendo un Movimiento vacio (id  de la Partida será nulo)
  * `AGREE`: El `AgenteJugador` guarda en su Tablero propio el movimiento del otro jugador y se prepara para hacer el suyo
* Movimiento: Mensaje enviado tras el `AGREE`. Puede ser:
  * `FAILURE`: Si el movimiento que manda el `AgenteTablero` no es correcto y por tanto, no se puede mover en consecuencia . La respuesta será un Movimiento nulo
  * `INFORM`: Si se puede realizar un Movimiento basado en el recibido . El resultado que se manda al `AgenteTablero` es un Movimiento 

#### Tratar mensajes fuera de secuencia

Hay que tener en cuenta que alguno de los `AgenteJugador` no responda de forma apropiada según los mensajes esperados en el protocolo y para ello hay que implementar el método `handleOutOfSequence(.)` para evitar que la tarea no finalice de forma apropiada que pueda provocar que el `AgenteTablero` termine finalizando de forma  errónea 

### 3.1.2 Protocolo FIPA-Subscribe

Para la resolución del envío de los mensajes al `AgenteConsola` utilizaremos el protocolo **FIPA-Subscribe** adaptado a nuestras necesidades quedando de esta forma:

#### Estructura

```mermaid
sequenceDiagram
	participant i as AgenteJugador
	participant r1 as AgenteTablero 
	i->>r1: SUBSCRIBE
	Note right of r1: Solicitud de subscrip<br/>ción


	alt rechazo
		r1-->>i: REFUSE (Respuesta)
	else problemas
		r1-->>i: FAILURE (Respuesta)
	else acepta
		r1-->>i: AGREE (Respuesta)
	end
	Note right of r1: Informes de subscrip<br/>ción
	r1-->>i: INFORM (MensajeSubscripcion)
	
    Note left of i: Respuesta es<br/> un String con  </br> Información
    Note left of i: MensajeSubscripcion <br/>es un String con  </br> Información de <br/>Ganador
```


### TareaIniciaSubscripcionPartida

Es la Tarea que en su constructor envia la solicitud de subscripcion y espera al mensaje de Aceptación y , si lo recibe, espera al mensaje de Subscripcion (`INFORM`). Esta tarea será la iniciada por el `AgenteJugador` para solicitar el ganador al `AgenteTablero` el ganador de las partidas a las que juegue. Cada vez que el `AgenteJugador` acepte una partida le enviara al `AgenteTablero` una solicitud . Una vez hecho esto, esperará al Informe del Ganador . Si este informe de Partida terminada tiene el **ID de Partida** una de las partidas que juega dicho jugador, este Jugador cerrará su Partida correspondiente (Recordemos que cada Jugador guardaba una Partida localmente, pues en los pasos de mensajes solo iba el **Movimiento** ).  

#### Respuestas a la solicitud de la subscripción

Vamos a centralizar todas las posibles respuestas en un único método que tratará los mensajes para la subscripción. En este caso, el metodo `handleAllResponses()` recibirá las respuestas de `AgenteTablero` a la solicitud del `AgenteJugador` :

- `REFUSE`: no se ha podido completar la solicitud de subscripción con el agente participante.
- `FAILURE`: se produce un problema cuando se intenta cancelar la subscripción.
- `AGREE`: se ha completado la subscripción de forma correcta. No haremos nada, simplemente esperaremos a que llegue el informe



#### Tratar los mensajes INFORM

Estos mensajes no se tratarán inmediatamente, es decir, dependerán de las condiciones de la subscripción y la forma en que se implemente, más adelante se mostrará, la tarea encargada del envío en los agentes con el rol participante. El método a implementar es `handleInform(ACLMessage inform)`. En este caso, haremos lo explicado anteriormente. Para cada mensaje de INFORM (respuesta a la subscripcion) que recibamos, comprobaremos si uno de estos agentes de la Partida Terminada somos nosotros, el `AgenteJugador` que lo recibe. Si es así , eliminará la Partida asociada a ese mensaje de Subscripcion de su lista de Partidas, para asi poder Aceptar y Jugar proposiciones de otras Partidas.

#### Tratar mensajes fuera de secuencia

Hay que tener en cuenta que alguno de los `AgenteTablero` no responda de forma apropiada según los mensajes esperados en el protocolo y para ello hay que implementar el método `handleOutOfSequence(.)` para evitar que la tarea no finalice de forma apropiada que pueda provocar que el `AgenteJugador` termine **finalizando** de forma *abrupta*. 


### TareaResponseSubscripcionPartida

Esta Tarea corresponderá al `AgenteTablero` para tratar las solicitudes de subscripcion del `AgenteJugador` .

Un elemento necesario para este tipo de tareas será el gestor para los elementos `Subscription` que se crean. Este elemento se explicará más adelante cuando se detallen las clases de utilidad que han sido necesarias definir para este ejemplo.

En este tipo de tareas debemos implementar los métodos que se encargan de:

- Responder a las solicitudes de subscripción que se soliciten.
- Responder a las solicitudes de cancelación de la subscripción que se soliciten.

#### Solicitud de subscripción

El método deberá responder apropiadamente a una solicitud de subscripción que se recibe. Las posibilidades son:

- `NOT_UNDERSTOOD`: no se entiende el contenido del mensaje. En nuestro ejemplo no se va a enviar ningún tipo de información y por tanto no se tratará este tipo de respuesta. Pero no en todos los problemas será así.
- `REFUSE`: se rechaza la solicitud de subscripción. En nuestro caso se tomará esta decisión si se produce algún problema en la tramitación de la subscripción, es decir, no se ha podido crear correctamente el elemento `Subscription` asociado a la petición.
- `AGREE`: se ha creado correctamente la subscripción.  Se añade la solicitud de Subscripcion al tratamiento de subscripcion de `GestorSubscripciones` y se manda un mensaje  `AGREE` al `AgenteJugador` pertinente

El método que se  implementa para realizar esto es `handleSubscription(ACLMessage subscription)`


#### Cancelación de la subscripción

Este método será necesario para la cancelación de la subscripción de un agente. Es importante mantener bien actualizadas las subscripciones para evitar problemas o gastar tiempo innecesario en la ejecución de los agentes. Esta método puede responder de la siguiente forma:

- `FAILURE`: si se produce algún problema en la cancelación de la subscripción. En este problema, y en muchos casos, estará determinado si se ha producido algún problema a la hora de eliminar el elemento `Subscription` en el gestor de subscripciones.
- `INFORM`: la subscripción se ha cancelado correctamente. Se desregistra /desuscribe al `AgenteJugador` pertinente de  `GestorSubscripciones` y se manda un mensaje al  `AgenteJugador` para verificarlo

El método que se  implementa para realizar esto es `handleCancel(ACLMessage cancel)`


### TareaEnvioSubscripcion

Esta es la tarea asignada al `AgenteTablero` que , una vez terminada la Partida, pone fin a la Subscripcion enviando a los  `AgenteJugador` subscritos a él el mensaje de Partida Terminada. La tarea se ha definido como `OneShotBehaviour`, ya que se hará solo un envio para cada partida terminada. Esta tarea, pues, enviará un `INFORM` al rol Iniciador de la Subscripcion, es decir a `AgenteJugador` . El contenido del mensaje será un String con el ID de la Partida que Finaliza , el jugador que ha ganado y la ficha con la que se ha hecho. 

A cada subscriptor se le enviará la misma lista de mensajes de subscripcion y , como dijimos anteriormente, estos analizarán el contenido del mensaje y actuarán en consecuencia, finalizando así cada una de las Partidas y permitiendo iniciarse otras.

## 3.2 Clases a Explicar

Una vez explicados los métodos de comunicación **JADE** usados, debemos hablar de los participantes como tal y de algunas clases que se han usado  para facilitar la ejecución correcta de la práctica.

### 3.2.1 `AgenteTablero`

Agente principal, pues es quien dirige las partidas . A este agente le corresponderán como ya dijimos antes las tareas:

* TareaProposeIniciaPartida -> Inicia el Propose
* TareaRequestIniciaMovimiento -> Inicia la Solicitud de Movimientos
* TareaResponseSubscripcionPartida -> Responde a las Subscripciones

Este Agente tiene las variables:

  * _**listaAgentes**_ : El `AgenteTablero` se subscribe a las Páginas Amarillas buscando `AgenteJugador`. Cada jugador que encuentra es añadido a esta Lista
  * _**listaJugadores**_: El `AgenteTablero` propone una partida a todos los `AgenteJugador` en la lista anterior. Obviamente, no todos aceptarán jugar una partida ( ya estén jugando otras 3 , algun tipo de error en el mensaje, etc). Por tanto, para poder coordinar los jugadores de las partidas, se crea esta Lista, que contendrá todos los `AgenteJugador` que hayan aceptado un Propose. 0
  * _**ultMovimiento**_ : Recordemos que el contenido del mensaje Request de **Movimiento** es el movimiento anterior hecho por  el adversario (en caso de ser el primer mmovimiento se enviará un Movimiento con posición nula, como dijimos en el apartado de "Request" ). Como maximo habrá 3 partidas, necesitarmos un ultimo Movimiento para cada cada una. Por tanto esto es una HashMap donde la clave es el IDPartida y el valor es el Ultimo Movimiento
  *  _**numPartidas**_ : Contador del numero de partidas que existen actualmente
  *  _**mensajeVictoria**_ : HashMap que contiene el mensaje `INFORM` de cada Partida, para mandarselo a cada agente.
  *  _**gsonUtil**_ : Objeto de la Clase GsonUtil (de la que se hablará despues ) que sirve para codificar y decodificar objetos en Strings para su envio y viceversa
  *  _**gestorSubscripciones**_ : Objeto de la clase `GestorSubscripciones`, de la que hablaremos debajo.
  *  _**tablero_gui**_ : HashMap en el que la variable clave es el **ID de Partida** y el valor es el Tablero asociado a dicha partida. Esto nos servirá para introducir el movimiento en el Tablero correcto, pues hay siempre 3 Partidas activas. La clase `Tablero`  se explicará mas abajo
  * _**info**_ : Objeto de la GUI `InformacionPartida` , que se explicará mas adelante, que nos permite enviar informacion sobre la partida para que el usuario la vea.

Este Agente dispondrá de la Interfaz/Interface `Tablero_Interface` para poder comunicarse con estas variables en las Tareas de comunicación que necesiten su acceso.

### 3.2.2 `AgenteJugador`

Agente dependiente del `AgenteTablero`, pues quien realiza lo que le pide el tablero (jugar, mandar movimientos, etc) . A este agente le corresponderán como ya dijimos antes las tareas:

* TareaProposeResponderPartida -> Responde al Propose
* TareaRequestResponseMovimiento -> Responde la Solicitud de Movimientos
* TareaIniciaSubscripcionPartida -> Inicia las Subscripciones. Este Agente se subscribirá a todos los `AgenteTablero` disponibles y les hará solicitudes de Subscripción

Debemos tener en cuenta que el `AgenteJugador` no podrá acceder a la clase Tablero, solo tendrá los **Movimientos** de ambos jugadores, por lo que tendrá que crearse un tablero local. En este caso, se ha decidido hacer una Matriz de "Color" (Constante ofrecida en el [código base](https://gitlab.com/ssmmaa/curso2019-20/practica1/-/archive/v0.1/practica1-v0.1.zip) ), que tendrá 3 variables : VACIO, AZUL y ROJO. Asignandosele a las casillas sin nada el valor VACIO y a los 2 `AgenteJugador` que vayan a realizar una acción uno de los otros dos colores. Esto explica porque en TareaRequestResponseMovimiento tendremos un método llamado `giveFicha(String ficha)` , ya que dependiendo de si es un movimiento inicial o un movimiento anterior lo que recibimos (como ya explicamos en el metodo **FIPA-Request**) asignará a este `AgenteJugador` un Color u otro. A partir de este tablero 

Este Agente tiene las variables:
  * _**ficha**_ : HashMap donde la clave es el IDPartida y el valor es el Color de ficha que tiene el `AgenteJugador` en cada Partida
  * _**tablero**_ : HashMap donde la clave es el IDPartida y el valor es el tablero local de cada Partida, para saber donde puede colocar ficha y donde ya hay fichas colocadas
  * _**idPartida**_ : Vector de los idPartidas en las que estamos jugando 
  * _**partidasTotales**_ : Numero de Partidas que tiene el `AgenteJugador` . Esto está hecho para que no se acepten mas de 3 partidas
  * _**subscripciones**_ : HashMap donde la clave es el AID del `AgenteTablero` y el valor es el la subscripcion al  `AgenteTablero` en forma de TareaInicioSubscripcion
  * _**listaTableros**_ : HashMap donde la clave es el IDPartida y el valor es el `AgenteTablero` que pide esa partida. Esto ayudará a las EEDD anteriores a seleccionar correctamente la partida


### 3.2.3 `GestorSubscripciones`
Clase obtenida del [Guion3](https://gitlab.com/ssmmaa/guionsesion3) de Sistemas Multiagentes , que permite agregar solicitudes de Subscripciones en un HashMap en el que la clave es el AID del `AgenteJugador` que solicita la subscripcion y el valor es la Subscripcion como tal. Al ser una clase que hereda de `SubscriptionManager` nos permite usarla en el protocolo **FIPA-Subscribe**
### 3.2.4 `GsonUtil`
Clase proporcionada en el [código base](https://gitlab.com/ssmmaa/curso2019-20/practica1/-/archive/v0.1/practica1-v0.1.zip) de la Práctica , que permite la codificación y decodificación de Objetos en Strings y viceversa (para su envio entre Tareas) , usando JSON y concretamente GSON (JSON de Google). Con esta clase podremos convertir cualquier objeto de envio en String , para poder enviarse 
### 3.2.5 `Tablero`
GUI que se nos proporciona en el [código base](https://gitlab.com/ssmmaa/curso2019-20/practica1/-/archive/v0.1/practica1-v0.1.zip) de la Práctica, que incluye el Tablero , junto a todos los datos que este necesita como los jugadores o las fichas. A esta clase se le ha incluido (aparte de Getter y Setters) un método llamado `addFichaTablero(AID s, Movimiento mov)` , que dado el AID del agente que manda el movimiento, y  la Partida (incluida en el Movimiento), busca su ficha y coloca esta en la posicion que se indica en el Movimiento, convirtiendo la posicion de Matriz a una en vector como asi se especifica en el Tablero, de esta forma:
  * Ya que la Posicion es 2D y las casillas del tablero 1D (Un vector en lugar de matriz) se convertirán las posiciones con la formula `pos = y + 3*x`


Es decir, este metodo se encarga de modificar el Tablero de cada idPartida , añadiendole los Movimientos que se les mandan al `AgenteTablero` .
### 3.2.6 `InformacionPartida`
 GUI que nos permite obtener de forma clara los datos de la partida. Dividido en 2 caracteristicas
 * Datos de la Partida: Mensajes de Propose, Request y comunicacion entre eestos
 * Datos de Finalizacion de Partida:  Mensajes de Subscribe, es decir, mensajes de INFORM de que partida ha terminado



