/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.practica1.agentes;

import es.uja.ssmmaa.curso1920.practica1.Constantes;
import es.uja.ssmmaa.curso1920.practica1.Constantes.Color;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.NUM_ENRAYA;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.NombreServicio.JUGADOR;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.TIME_OUT;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.TipoServicio.SISTEMA;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.TipoServicio.TRES_EN_RAYA;
import es.uja.ssmmaa.curso1920.practica1.gui.InformacionPartida;
import es.uja.ssmmaa.curso1920.practica1.tareas.Partida;
import es.uja.ssmmaa.curso1920.practica1.tareas.SubscripcionDF;
import es.uja.ssmmaa.curso1920.practica1.tareas.TareaIniciaSubscripcionPartida;
import es.uja.ssmmaa.curso1920.practica1.tareas.TareaProposeResponderPartida;
import es.uja.ssmmaa.curso1920.practica1.tareas.TareaRequestResponseMovimiento;
import es.uja.ssmmaa.curso1920.practica1.tareas.TareaSubscripcionDF;
import es.uja.ssmmaa.curso1920.practica1.util.Posicion;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author nervi
 */
public class AgenteJugador extends Agent implements Partida, SubscripcionDF {

    //Variables del agente
    private HashMap<Integer,Color> ficha;
    private HashMap<Integer,ArrayList<ArrayList<Color>>> tablero;
    private ArrayList<Integer> idPartida;
    private int partidasTotales;
    private HashMap<AID, TareaIniciaSubscripcionPartida> subscripciones;
    private HashMap<Integer,AID> listaTableros; // AgenteTablero de cada partida 
    private int numPartidas;
    @Override
    protected void setup() {
        //Inicialización de las variables del agente
        tablero =  new HashMap<>();
        ficha = new HashMap<>();
        idPartida = new ArrayList<>();
        partidasTotales = 0;
        subscripciones = new HashMap<>();
        listaTableros = new HashMap<>();
        numPartidas=0;
        //Configuración del GUI
        //Registro del agente en las Páginas Amarrillas
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType(TRES_EN_RAYA.name());
        sd.setName(JUGADOR.name());
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }

        //Registro de la Ontología
        System.out.println("Se inicia la ejecución del agente: " + this.getName());
        //Añadir las tareas principales

        
        // Plantilla del mensaje de suscripción
        //Suscripción al servicio de páginas amarillas
        //Para localiar a los Tablero
        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription templateSd = new ServiceDescription();
        templateSd.setType(SISTEMA.name());
        templateSd.setName(Constantes.NombreServicio.TABLERO.name());
        template.addServices(templateSd);
        addBehaviour(new TareaSubscripcionDF(this, template));
   

        // Plantilla para la tarea ProponerPartida
        MessageTemplate mtProponerPartida = 
                MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_PROPOSE);
        addBehaviour(new TareaProposeResponderPartida(this, mtProponerPartida));
        
   
     MessageTemplate msgTemplate = MessageTemplate.and(
                MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST),
                MessageTemplate.MatchPerformative(ACLMessage.REQUEST));
        addBehaviour(new TareaRequestResponseMovimiento(this, msgTemplate)); 
        
    }   

    @Override
    protected void takeDown() {
        //Eliminar registro del agente en las Páginas Amarillas
        try {
            cancelarSubscripcion(this.getAID());
            DFService.deregister(this);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
        //Liberación de recursos, incluido el GUI
        //Despedida
        System.out.println("Finaliza la ejecución del agente: " + this.getName());
    }

    //Métodos de trabajo del agente
    //Clases internas que representan las tareas del agente
    @Override
    public ArrayList<ArrayList<Constantes.Color>> getTablero(int id) {
        return tablero.get(id);
    }
    

    
    @Override
    public Constantes.Color getColor(int idP) {
        return ficha.get(idP);
    }

    @Override
    public void setColor(int idP, Color f) {
        ficha.put(idP, f);
    }

    @Override
    public void setPosTablero(int id, Color f, Posicion pos) {
        ArrayList<ArrayList<Color>> tab = tablero.get(id);
        if (tab.get(pos.getX()).get(pos.getY()) == Color.VACIO) {
            ArrayList<Color> a = tab.get(pos.getX());
            a.add(pos.getY(), f);
            tab.add(pos.getX(), a);
            tablero.put(id, tab);
        }
    }

    @Override
    public void addIdPartida(int i) {
        idPartida.add(i);
    }
    @Override
    public ArrayList getIdPartida() {
        return idPartida;
    }

    private void crearSubscripcion(AID agente) {
        //Creamos el mensaje para lanzar el protocolo Subscribe
        ACLMessage msg = new ACLMessage(ACLMessage.SUBSCRIBE);
        msg.setProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE);
        msg.setReplyByDate(new Date(System.currentTimeMillis() + TIME_OUT));
        msg.setSender(this.getAID());
        msg.addReceiver(agente);

        // Añadimos la tarea de suscripción
        TareaIniciaSubscripcionPartida sub = new TareaIniciaSubscripcionPartida(this, msg);
        subscripciones.put(agente, sub);
        addBehaviour(sub);
    }
    
    
    private void cancelarSubscripcion(AID agente) {

        TareaIniciaSubscripcionPartida sub = subscripciones.remove(agente);
        if (sub != null) {
            String msg = this.getLocalName();
            // setMensaje(msg); Esto será para la interfaz
            sub.cancel(agente, true);
        }
    }
    @Override
    public int getNumPartidas() {
        return numPartidas;
    }
    @Override
    public void setNumPartidas(int numPartidas) {
        this.numPartidas = numPartidas;
    }



    @Override
    public void setPartidasTotales(int n) {
        partidasTotales = n;
    }

    @Override
    public int getPartidasTotales() {
        return partidasTotales;
    }

    @Override
    public void addAgentTablero(int idP, AID ag) {
        listaTableros.put(idP, ag);
    }

    @Override
    public AID removeAgentTablero(int idP) {
      return listaTableros.remove(idP);
    }

    @Override
    public void addAgent(AID agente, Constantes.NombreServicio servicio) {
        switch (servicio) {
            case TABLERO:
                crearSubscripcion(agente);
                break;
        }
    }

    @Override
    public boolean removeAgent(AID agente, Constantes.NombreServicio servicio) {
        switch (servicio) {
            case TABLERO:
                cancelarSubscripcion(agente);
                return true;
        }
        return false;
    }
    @Override
    public void setTablero(int idP, ArrayList<ArrayList<Color>> a){
        tablero.put(idP, a);
    }
    
}
