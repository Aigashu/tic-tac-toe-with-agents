/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.practica1.agentes;

import es.uja.ssmmaa.curso1920.practica1.Constantes;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.CASILLAS;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.COLORES;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.ESPERA;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.ESTADO;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.MAX_PARTIDAS;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.NombreServicio.JUGADOR;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.NombreServicio.TABLERO;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.TIME_OUT;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.TipoServicio.SISTEMA;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.TipoServicio.TRES_EN_RAYA;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.aleatorio;
import es.uja.ssmmaa.curso1920.practica1.gui.InformacionPartida;
import es.uja.ssmmaa.curso1920.practica1.gui.Tablero;
import es.uja.ssmmaa.curso1920.practica1.tareas.SubscripcionDF;
import es.uja.ssmmaa.curso1920.practica1.tareas.Tablero_Interface;
import es.uja.ssmmaa.curso1920.practica1.tareas.TareaEnvioSubscribcion;
import es.uja.ssmmaa.curso1920.practica1.tareas.TareaRepitePropose;
import es.uja.ssmmaa.curso1920.practica1.tareas.TareaRequestIniciaMovimiento;
import es.uja.ssmmaa.curso1920.practica1.tareas.TareaResponseSubscripcionPartida;
import es.uja.ssmmaa.curso1920.practica1.tareas.TareaSubscripcionDF;
import es.uja.ssmmaa.curso1920.practica1.util.GestorSubscripciones;
import es.uja.ssmmaa.curso1920.practica1.util.GsonUtil;
import es.uja.ssmmaa.curso1920.practica1.util.Jugador;
import es.uja.ssmmaa.curso1920.practica1.util.Movimiento;
import es.uja.ssmmaa.curso1920.practica1.util.Posicion;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.FailureException;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.SubscriptionResponder;
import jade.proto.SubscriptionResponder.Subscription;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author nervi
 */
public class AgenteTablero extends Agent implements SubscripcionDF, Tablero_Interface {

    //Variables del agente
    private ArrayList<AID> listaAgentes; //Agentes a los que se les mandará el PROPOSE
    private ArrayList<AID> listaJugadores; //AGENTES QUE ACEPTAN PROPOSE
    private HashMap<Integer, Movimiento> ultMovimiento; //para los ultimos movimientos de cada partida. Sera lo que mande 
    private int numPartidas;
    private HashMap<Integer, String> mensajeVictoria;

    private GsonUtil gsonUtil;
    private GestorSubscripciones gestorSubscripciones;
    private Boolean proposedone;
    private Boolean partidaTerminada; //Esto servirá para mandar la respuesta a la subscripcion
    private HashMap<Integer, Tablero> tablero_gui; // como hay 3 tableros pues eso
    private InformacionPartida info;

    @Override
    protected void setup() {
        //Inicialización de las variables del agente
        listaAgentes = new ArrayList<AID>();
        listaJugadores = new ArrayList<AID>();
        listaJugadores.clear();
        info = new InformacionPartida();
        info.setVisible(true);
        ultMovimiento = new HashMap<Integer, Movimiento>();
        mensajeVictoria = new HashMap<>();
        numPartidas = 0;
        proposedone = false;
        partidaTerminada = false;
        gestorSubscripciones = new GestorSubscripciones();
        for (int i = 0; i < MAX_PARTIDAS; i++) {
            ultMovimiento.put(i, null);
        }
        tablero_gui = new HashMap<>();
        gsonUtil = new GsonUtil();

        //Configuración del GUI
        //Registro del agente en las Páginas Amarrillas
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType(SISTEMA.name());
        sd.setName(TABLERO.name());
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
        //Registro de la Ontología

        System.out.println("Se inicia la ejecución del agente: " + this.getName());
        //Añadir las tareas principales

        // Plantilla del mensaje de suscripción
        //Suscripción al servicio de páginas amarillas
        //Para localizar a los agentes JUGADOR
        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription templateSd = new ServiceDescription();
        templateSd.setType(TRES_EN_RAYA.name());
        templateSd.setName(Constantes.NombreServicio.JUGADOR.name());
        template.addServices(templateSd);
        addBehaviour(new TareaSubscripcionDF(this, template));
        //Propose partida
        addBehaviour(new TareaRepitePropose(this, ESPERA));

        //Subscripcion
        MessageTemplate plantilla;
        plantilla = MessageTemplate.and(
                MessageTemplate.not(
                        MessageTemplate.or(MessageTemplate.MatchSender(getDefaultDF()),
                                MessageTemplate.MatchSender(getAMS()))),
                MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE));
        addBehaviour(new TareaResponseSubscripcionPartida(this, plantilla, gestorSubscripciones));

    }

    @Override
    protected void takeDown() {
        //Eliminar registro del agente en las Páginas Amarillas

        //Liberación de recursos, incluido el GUI
        //Despedida
        System.out.println("Finaliza la ejecución del agente: " + this.getName());
    }

    //Métodos de trabajo del agente
    //Clases internas que representan las tareas del agente
    @Override
    public void addAgent(AID agente, Constantes.NombreServicio servicio) {
        switch (servicio) {
            case JUGADOR:
                listaAgentes.add(agente);
                break;
        }
    }

    @Override
    public boolean removeAgent(AID agente, Constantes.NombreServicio servicio) {
        switch (servicio) {
            case JUGADOR:
                listaAgentes.remove(agente);
                return true;
        }
        return false;
    }

    @Override
    public HashMap<Integer, String> getMensajeVictoria() {
        return mensajeVictoria;
    }

    @Override
    public ArrayList<AID> getListaJugadores() {
        return listaJugadores;
    }

    public void setListaJugadores(ArrayList<AID> listaJugadores) {
        this.listaJugadores = listaJugadores;
    }

    @Override
    public HashMap<Integer, Tablero> getTablero_gui() {
        return tablero_gui;
    }

    @Override
    public InformacionPartida getInfo() {
        return info;
    }

    @Override
    public void setMensaje(AID sender, Movimiento msg) {

        int idP = Integer.parseInt(msg.getIdPartida());
        Tablero t = tablero_gui.get(idP);
        t.addFichaTablero(sender, msg);
        tablero_gui.put(idP, t);
    }

    @Override
    public Boolean compruebaVictoria(ACLMessage msg, Movimiento mov, Boolean empate) {

        String jug = mov.getJugador().getNickName();
        Tablero t = tablero_gui.get(Integer.parseInt(mov.getIdPartida()));
        Icon icono;
        Icon iconoEmpate = new ImageIcon(getClass().getResource("/" + ESTADO[0].getFichero()));
        String ficha;
        if (t.getListaJugadores()[0].equals(jug)) {
            icono = t.getFicha()[0].getIcon();
            ficha = " X ";
        } else {
            icono = t.getFicha()[1].getIcon();
            ficha = " O ";
        }
        if ((t.getCasilla()[0].getIcon() == icono && t.getCasilla()[1].getIcon() == icono && t.getCasilla()[2].getIcon() == icono)
                || (t.getCasilla()[0].getIcon() == icono && t.getCasilla()[3].getIcon() == icono && t.getCasilla()[6].getIcon() == icono)
                || (t.getCasilla()[3].getIcon() == icono && t.getCasilla()[4].getIcon() == icono && t.getCasilla()[5].getIcon() == icono)
                || (t.getCasilla()[6].getIcon() == icono && t.getCasilla()[7].getIcon() == icono && t.getCasilla()[8].getIcon() == icono)
                || (t.getCasilla()[1].getIcon() == icono && t.getCasilla()[4].getIcon() == icono && t.getCasilla()[7].getIcon() == icono)
                || (t.getCasilla()[2].getIcon() == icono && t.getCasilla()[5].getIcon() == icono && t.getCasilla()[8].getIcon() == icono)
                || (t.getCasilla()[0].getIcon() == icono && t.getCasilla()[4].getIcon() == icono && t.getCasilla()[8].getIcon() == icono)
                || (t.getCasilla()[2].getIcon() == icono && t.getCasilla()[4].getIcon() == icono && t.getCasilla()[6].getIcon() == icono)) {

            String victoria = msg.getSender().getLocalName() + "# ha ganado la partida: #" + mov.getIdPartida() + "# con Ficha: " + ficha + "\n";
            mensajeVictoria.put(Integer.parseInt(mov.getIdPartida()), victoria);
            info.addInfoSubscripcion(victoria);
            
            setPartidaTerminada(true);
            addBehaviour(new TareaEnvioSubscribcion(this, Integer.parseInt(mov.getIdPartida())));
            return true;
        }

        int cont = 0;
        for (int i = 0; i < t.getCasilla().length; i++) {
            if (t.getCasilla()[i].getIcon() != iconoEmpate) {
                cont++;
            }
        }
        if (cont == CASILLAS) {
            empate = true;
        }

        return false;

    }

    @Override
    public void empezarPartida(int id, String[] players) {
        String ide = id + "";
        tablero_gui.put(id, new Tablero(ide, players));
        setNumPartidas(getNumPartidas() + 1);
        tablero_gui.get(id).setVisible(true);

        //Inicia la partida
        ArrayList<String> jug = new ArrayList<>();
        for (int i = 0; i < getListaJugadores().size(); i++) {
            AID a = (AID) getListaJugadores().get(i);
            jug.add(a.getLocalName());
        }

        for (int j = 0; j < jug.size(); j++) {
            if (players[0].equals(jug.get(j))) {
                //Plantilla de Msg Inicio: Solicita primer movimiento
                Movimiento mov = new Movimiento(id + "");
                mov.setFicha(COLORES[aleatorio.nextInt(1)]);
                mov.setJugador(new Jugador(getLocalName()));
                mov.setPosicion(new Posicion(-1, -1));

                ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
                msg.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
                msg.setReplyByDate(new Date(System.currentTimeMillis() + TIME_OUT));
                msg.addReceiver((AID) getListaJugadores().get(j));
                msg.setContent(gsonUtil.encode(mov, Movimiento.class));
                addBehaviour(new TareaRequestIniciaMovimiento(this, msg));
            }
        }

    }

    @Override
    public void setUltMov(int idPartida, Movimiento mov) {
        ultMovimiento.put(idPartida, mov);
    }

    public Movimiento getUltMov(int id) {
        return ultMovimiento.get(id);
    }

    @Override
    public void addAgentePropose(AID agente) {
        listaJugadores.add(agente);
        //Con esto eliminamos repetidos
        HashSet s = new HashSet<>(listaJugadores);
        listaJugadores = new ArrayList<>(s);

    }

    @Override
    public ArrayList<AID> getListaAgentes() {
        return listaAgentes;
    }

    @Override
    public int getNumPartidas() {
        return numPartidas;
    }

    @Override
    public void setNumPartidas(int n) {
        numPartidas = n;
    }

    @Override
    public Boolean getDonePropose() {
        return proposedone;
    }

    @Override
    public void setProposeDone(Boolean d) {
        proposedone = d;
    }

    @Override
    public Boolean getPartidaTerminada() {
        return partidaTerminada;
    }

    @Override
    public void setPartidaTerminada(Boolean d) {
        partidaTerminada = d;
    }

    @Override
    public boolean register(SubscriptionResponder.Subscription s) throws RefuseException, NotUnderstoodException {
        // Guardamos la suscripción asociada al agente que la solita
        gestorSubscripciones.register(s);
        return true;
    }

    @Override
    public boolean deregister(SubscriptionResponder.Subscription s) throws FailureException {
        // Eliminamos la suscripción asociada a un agente
        gestorSubscripciones.deregister(s);
        return true;
    }

    @Override
    public Subscription getSubscripcion(AID sender) {
        return gestorSubscripciones.getSubscripcion(sender);
    }

    @Override
    public boolean haySubscripcion(AID sender) {
        return (gestorSubscripciones.getSubscripcion(sender) != null);
    }

    @Override
    public GestorSubscripciones getGestorSubscribciones() {
        return gestorSubscripciones;
    }
    @Override
    public void terminaPartida(int idP){
        try {
            Thread.sleep(1500);
            tablero_gui.get(idP).dispose();
            tablero_gui.remove(idP);
        } catch (InterruptedException ex) {
            Logger.getLogger(AgenteTablero.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
