/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.practica1.gui;

import static es.uja.ssmmaa.curso1920.practica1.Constantes.CASILLAS;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.ESTADO;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.EstadoCasilla.LIBRE;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.JUGADORES;
import es.uja.ssmmaa.curso1920.practica1.Constantes.Jugador;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.PATH;
import es.uja.ssmmaa.curso1920.practica1.util.GsonUtil;
import es.uja.ssmmaa.curso1920.practica1.util.Movimiento;
import jade.core.AID;
import jade.lang.acl.ACLMessage;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Image;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

/**
 *
 * @author pedroj
 */
public class Tablero extends javax.swing.JFrame {

    private final String idPartida;
    private final String[] listaJugadores;
    private final ImageIcon[] img;
    private final JLabel[] jugador;
    private final JLabel[] ficha;
    private final JLabel[] casilla;
    private final JPanel cabecera;
    private final JPanel tablero;
    private JLabel[] casillasOcupadas;

    /**
     * Creates new form NewJFrame
     */
    public Tablero(String idPartida, String[] jugadores) {
        Icon icono;

        initComponents();
        this.idPartida = "Partida iD: " + idPartida;
        this.setTitle(this.idPartida);
        this.listaJugadores = jugadores;

        // Cambiamos el Layout de la ventana principal
        this.getContentPane().setLayout(new BorderLayout());
        JPanel principal = new JPanel();
        principal.setLayout(new BorderLayout());
        principal.setBorder(BorderFactory.createEmptyBorder(3, 5, 5, 5));
        this.add(principal);

        // Crear las imágenes del juego para la representación en el tablero
        img = new ImageIcon[ESTADO.length];
        for (int i = 0; i < ESTADO.length; i++) //img[i] = new ImageIcon(PATH + ESTADO[i].getFichero());
        {
            img[i] = new ImageIcon(getClass().getResource("/" + ESTADO[i].getFichero()));
        }
        // Inicializamos los componentes del tablero
        jugador = new JLabel[JUGADORES.length];
        ficha = new JLabel[JUGADORES.length];
        casilla = new JLabel[CASILLAS];
        cabecera = new JPanel();
        tablero = new JPanel();
        casillasOcupadas = new JLabel[CASILLAS];
        // Añadimos los componentes a la cabecera
        cabecera.setLayout(new GridLayout(2, 2, 2, 2));
        cabecera.setBorder(BorderFactory.createEmptyBorder(2, 2, 5, 2));
        for (int i = 0; i < JUGADORES.length; i++) {
            jugador[i] = new JLabel();
            jugador[i].setText(listaJugadores[i]);
            jugador[i].setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
            jugador[i].setHorizontalAlignment(JLabel.CENTER);
            cabecera.add(jugador[i]);
        }
        for (int i = 0; i < JUGADORES.length; i++) {
            ficha[i] = new JLabel();
            icono = new ImageIcon(img[i + 1].getImage().getScaledInstance(30, 30,
                    Image.SCALE_DEFAULT));
            ficha[i].setIcon(icono);
            ficha[i].setHorizontalAlignment(JLabel.CENTER);
            cabecera.add(ficha[i]);
        }
        principal.add(cabecera, BorderLayout.PAGE_START);

        // Añadimos el tablero
        tablero.setLayout(new GridLayout(3, 3, 4, 4));
        tablero.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
        icono = new ImageIcon(img[LIBRE.ordinal()].getImage().getScaledInstance(100, 100,
                Image.SCALE_DEFAULT));
        for (int i = 0; i < 9; i++) {
            casilla[i] = new JLabel();
            casilla[i].setIcon(icono);
            casilla[i].setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
            casilla[i].setHorizontalAlignment(JLabel.CENTER);
            casillasOcupadas[i] = null;
            tablero.add(casilla[i]);
        }
        principal.add(tablero, BorderLayout.CENTER);

        this.pack();
        this.repaint();
    }

    public String[] getListaJugadores() {
        return listaJugadores;
    }

    public JLabel[] getFicha() {
        return ficha;
    }

    public JLabel[] getCasilla() {
        return casilla;
    }
    

    public void addFichaTablero(AID s, Movimiento mov) {
        int pos = (mov.getPosicion().getY()) + 3 * (mov.getPosicion().getX());
        if (casillasOcupadas[pos] == null) {
            String un = jugador[0].getText();
            String dos = s.getLocalName();
            if (un.equals(dos)) {
                casilla[pos].setIcon(ficha[0].getIcon());
                casillasOcupadas[pos] = ficha[0]; //La ficha que este ahi ( X o O)
                tablero.remove(pos);
                tablero.add(casilla[pos],pos);
            } else {
                casilla[pos].setIcon(ficha[1].getIcon());
                casillasOcupadas[pos] = ficha[1]; //La ficha que este ahi ( X o O)
                tablero.remove(pos);
                tablero.add(casilla[pos],pos);
            }
            this.pack();
            this.repaint();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setAlwaysOnTop(true);
        setResizable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 312, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
