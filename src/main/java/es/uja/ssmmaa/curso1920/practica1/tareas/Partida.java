/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.practica1.tareas;

import es.uja.ssmmaa.curso1920.practica1.Constantes.Color;
import es.uja.ssmmaa.curso1920.practica1.util.Posicion;
import jade.core.AID;
import java.util.ArrayList;

/**
 *
 * @author nervi
 */
public interface Partida {
    public ArrayList<ArrayList<Color>> getTablero(int id);
    public Color getColor(int idP);
    public void setColor(int idP, Color f);
    public void setPosTablero(int id, Color f, Posicion pos);
    public void addIdPartida(int i);
    public ArrayList getIdPartida();
    public void setPartidasTotales( int n);
    public int getPartidasTotales();
    public void addAgentTablero(int idP, AID ag);
    public AID removeAgentTablero(int idP);
    public void setTablero(int idP, ArrayList<ArrayList<Color>> a);
    public int getNumPartidas();
    public void setNumPartidas(int numPartidas);
}
