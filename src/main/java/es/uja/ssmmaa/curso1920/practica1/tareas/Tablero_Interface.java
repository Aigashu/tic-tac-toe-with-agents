/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.practica1.tareas;

import es.uja.ssmmaa.curso1920.practica1.gui.InformacionPartida;
import es.uja.ssmmaa.curso1920.practica1.gui.Tablero;
import es.uja.ssmmaa.curso1920.practica1.util.GestorSubscripciones;
import es.uja.ssmmaa.curso1920.practica1.util.Movimiento;
import jade.core.AID;
import jade.domain.FIPAAgentManagement.FailureException;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.proto.SubscriptionResponder;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author nervi
 */
public interface Tablero_Interface<T> {
    public void setMensaje(AID sender, Movimiento msg);
    public void setUltMov( int idPartida, Movimiento mov);
    public Movimiento getUltMov( int id);
    public void addAgentePropose( AID agente);
    public ArrayList<AID> getListaAgentes();
    public int getNumPartidas();
    public void setNumPartidas(int n);
    public ArrayList<AID> getListaJugadores();
    public Boolean getDonePropose();
    public void setProposeDone(Boolean d);
    public Boolean getPartidaTerminada();
    public void setPartidaTerminada(Boolean d);
    public InformacionPartida getInfo() ;
    public Boolean compruebaVictoria(ACLMessage msg, Movimiento mov, Boolean empate);
    public HashMap<Integer, String> getMensajeVictoria();
    public void empezarPartida(int id,String [] players);
    public HashMap<Integer, Tablero> getTablero_gui();
    public void terminaPartida (int idP);
     
    public boolean register(SubscriptionResponder.Subscription s) throws RefuseException, NotUnderstoodException ;
    public boolean deregister(SubscriptionResponder.Subscription s) throws FailureException;
    public SubscriptionResponder.Subscription getSubscripcion( AID sender );
    public boolean haySubscripcion(AID sender);
    public GestorSubscripciones getGestorSubscribciones();
}
