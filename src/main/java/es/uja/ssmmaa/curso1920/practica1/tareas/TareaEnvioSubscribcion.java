/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.practica1.tareas;

import es.uja.ssmmaa.curso1920.practica1.util.GestorSubscripciones;
import es.uja.ssmmaa.curso1920.practica1.util.GsonUtil;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.proto.SubscriptionResponder.Subscription;
import java.util.HashMap;
import java.util.List;

/**
 * Tarea que se repetirá cada intervalo de tiempo ESPERA para comprobar si hay
 * mensajes que enviar a agentes que previamente han formalizado una
 * subscripción. El tipo de mensaje que se envía es MensajeConsola.
 *
 * @author pedroj
 */
public class TareaEnvioSubscribcion extends OneShotBehaviour {

    private final Tablero_Interface agenteTablero;
    private final GsonUtil gsonUtil;
    private int ID;

    public TareaEnvioSubscribcion(Agent a, int ID) {
        super(a);

        this.agenteTablero = (Tablero_Interface) a;
        this.gsonUtil = new GsonUtil();
        this.ID = ID;
    }

    /**
     * Tarea que explora cíclicamente si tenemos mensajes pendientes que enviar
     * a todas las subscripciones con AgenteConsola activas.
     */
    @Override
    public void action() {
        GestorSubscripciones gestorSub = agenteTablero.getGestorSubscribciones();

        // Hay mensajes pendientes y subscripciones activas
        if (!gestorSub.isEmpty()) // Para todas las subscripciones activas
        {
            for (Subscription subscripcion : gestorSub.values()) {
                enviar(subscripcion, subscripcion.getMessage().getSender());
            }
            //Tras mandar todos los mensajes de esa partida, se elimina
            agenteTablero.getMensajeVictoria().remove(ID);
            agenteTablero.terminaPartida(ID);
            
        }

    }

    /**
     * Envia los mensajes pendientes a las subscripciones de las consolas
     * conocidas
     *
     * @param subscripcion Subscripción a un AgenteConsola acriva
     * @param mensajes Lista de MensajeConsola a enviar
     */
    private void enviar(Subscription subscripcion, AID sender) {
        HashMap<Integer, String> listaVictorias = agenteTablero.getMensajeVictoria();
        int Partida = Integer.parseInt(listaVictorias.get(ID).split("#")[2]);
        ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
        msg.addReceiver(sender);
        msg.setContent(gsonUtil.encode(listaVictorias.get(ID), String.class));

        // Enviamos el mensaje al suscriptor
        subscripcion.notify(msg);
        
    }
}
