/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.practica1.tareas;

import es.uja.ssmmaa.curso1920.practica1.util.GsonUtil;
import es.uja.ssmmaa.curso1920.practica1.util.Movimiento;
import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.proto.SubscriptionInitiator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

/**
 *
 * @author nervi
 */
public class TareaIniciaSubscripcionPartida extends SubscriptionInitiator {
    private GsonUtil gsonUtil;
    private String msgInfo;
    private Partida agenteJugador;
    public TareaIniciaSubscripcionPartida(Agent a, ACLMessage msg) {
        super(a, msg);
        gsonUtil = new GsonUtil();
        msgInfo = null;
        agenteJugador= (Partida) a;
    }
      /**
     * Resultado de la solicitud de subscripción enviada a los agentes
     * participantes del protocolo Subscribe. Se genera un MensajeConsola para
     * el agente.
     * @param responses 
     *          Mensaje ACL con el resultado a la solicitud de subscripción.
     */
    @Override
    protected void handleAllResponses(Vector responses) {
        Iterator it = responses.iterator();
        
        while (it.hasNext()) {
            
            ACLMessage msg = (ACLMessage) it.next();
           
            try {
                msgInfo =  (String) gsonUtil.decode(msg.getContent(),String.class);
            
                switch ( msg.getPerformative() ) {
                    case ACLMessage.REFUSE:
                    case ACLMessage.FAILURE:
                    case ACLMessage.AGREE:
                        //Esperamos al Inform
                        break;
                    default:
                        System.out.println("Mensaje desconocido");
                        break;
                }
            } catch ( Exception e ) {
                System.out.print(msg.getSender().getLocalName() +
                                          " El contenido del mensaje es incorrecto\n\t"
                                          + e);
            }
            
            
        }
    }

    /**
     * Recepción de los MensajeConsola de los agentes que aceptaron la subscripción
     * cuando tienen alguno disponible y se le pasará al agente.
     * @param inform 
     *          Mensaje ACL que tiene el MensajeConsola.
     */
    @Override
    protected void handleInform(ACLMessage inform) {
       String info;
        try {
            msgInfo = (String) gsonUtil.decode(inform.getContent(), String.class);
            int Partida = Integer.parseInt(msgInfo.split("#")[2]);
            String ag =  msgInfo.split("#")[0];
            Boolean ganado = false;
            ArrayList<Integer> listaPartidas = agenteJugador.getIdPartida();
            for( int i =0; i< listaPartidas.size();i++){
                if(listaPartidas.get(i) == Partida){
                    if(myAgent.getAID().getLocalName().equals(ag)){
                        info= "Agente: "+   myAgent.getAID().getLocalName() + " he ganado la Partida "+ Partida;
                        System.out.println(info);
                        agenteJugador.removeAgentTablero(i);
                        ganado = true;
                        break;
                    }
                }
            }
            if (!ganado){
                info = "Agente: "+ myAgent.getAID().getLocalName() + " He perdido  todas las partidas";
                System.out.println(info);
            }
            agenteJugador.setPartidasTotales(agenteJugador.getPartidasTotales()-1);
        } catch ( Exception e ) {
               System.out.println("Excepcion , no se entiende contenido de mensaje Inform");
        }
        
      
    }
    
    /**
     * Se tratará cualquier mensaje que no llegue en la secuencia esperada para
     * el protocolo Subscribe.
     * @param msg 
     *          Mensaje ACL que ha llegado fuera de la secuencia del protocolo.
     */
    @Override
    protected void handleOutOfSequence(ACLMessage msg) {
        System.out.println("Mensaje fuera de secuencia");
    } 
}
