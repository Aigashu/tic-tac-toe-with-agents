    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.practica1.tareas;


import static es.uja.ssmmaa.curso1920.practica1.Constantes.ESPERA;
import es.uja.ssmmaa.curso1920.practica1.util.GsonUtil;
import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.proto.ProposeInitiator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

/**
 *
 * @author nervi
 */
public class TareaProposeIniciaPartida extends ProposeInitiator {

    private GsonUtil gsonUtil;
    private Tablero_Interface agenteTablero;
    private ArrayList<AID> parJugadores; // para que la partida vaya de dos en dos

    public TareaProposeIniciaPartida(Agent a, ACLMessage msg) {
        super(a, msg);
        gsonUtil = new GsonUtil();
        agenteTablero = (Tablero_Interface) a;
        parJugadores = new ArrayList<>();
    }

    @Override
    protected void handleOutOfSequence(ACLMessage msg) {
        // Ha llegado un mensaje fuera de la secuencia del protocolo
        System.out.println("El agente: " + msg.getSender().getName()
                + "\n ha enviado el siguiente mensaje de Propose ");
    }

    @Override
    protected void handleAllResponses(Vector responses) {

        ACLMessage msg;
        Iterator it = responses.iterator();

        // Recorremos todas las respuestas recibidas
        while (it.hasNext()) {
            msg = (ACLMessage) it.next();
            if (msg.getPerformative() == ACLMessage.ACCEPT_PROPOSAL) {
                parJugadores.add(msg.getSender());
                
                // Obtenemos el jugador que acepta jugar la partida

                agenteTablero.addAgentePropose(msg.getSender());
               
                if (parJugadores.size() == 2) {
                    String[] jugadores = {"", ""};
                    jugadores[0] = parJugadores.get(0).getLocalName();
                    jugadores[1] = parJugadores.get(1).getLocalName();
                    agenteTablero.empezarPartida(agenteTablero.getNumPartidas(), jugadores);
                    parJugadores.clear();
                
                }
            } else {
               
                System.out.println("El agente " + msg.getSender().getLocalName() + " ha rechazado la partida\n ");
            }

        }
    }
}
