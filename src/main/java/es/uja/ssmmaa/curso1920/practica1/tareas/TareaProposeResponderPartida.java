/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.practica1.tareas;

import es.uja.ssmmaa.curso1920.practica1.Constantes;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.MAX_PARTIDAS;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.NUM_ENRAYA;
import es.uja.ssmmaa.curso1920.practica1.util.GsonUtil;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.ProposeResponder;
import java.util.ArrayList;

/**
 *
 * @author nervi
 */
public class TareaProposeResponderPartida extends ProposeResponder {

    private GsonUtil gsonUtil;
    private Partida agenteJugador;

    public TareaProposeResponderPartida(Agent agente, MessageTemplate mt) {
        super(agente, mt);
        gsonUtil = new GsonUtil();
        agenteJugador = (Partida) agente;
    }

    @Override
    protected ACLMessage prepareResponse(ACLMessage propose) throws NotUnderstoodException, RefuseException {

        ACLMessage respuesta = propose.createReply();

        // Decidimos si aceptamos una nueva nuevaPartida
        if (agenteJugador.getPartidasTotales() < MAX_PARTIDAS) {
            // Contabilizamos las partidas activas y las que tenemos
            // con un agente Policia
            CreaTablero(agenteJugador.getNumPartidas());
            agenteJugador.setPartidasTotales(agenteJugador.getPartidasTotales() + 1);
            agenteJugador.setNumPartidas(agenteJugador.getNumPartidas()+1);
            // Enviamos la respuesta de aceptación
            respuesta.setPerformative(ACLMessage.ACCEPT_PROPOSAL);

            // Creamos el contenido del mensaje
            respuesta.setContent(gsonUtil.encode("", String.class));

           
            
            
            return respuesta;
        } else {
            // Muchas partidas
            respuesta.setPerformative(ACLMessage.REJECT_PROPOSAL);

         
            return respuesta;
        }
    }
    
    private void CreaTablero(int idP){
         ArrayList<ArrayList<Constantes.Color>> a = new ArrayList<>();
            for (int x = 0; x < NUM_ENRAYA; x++) {
                ArrayList<Constantes.Color> e = new ArrayList<>();
                for (int y = 0; y < NUM_ENRAYA; y++) {
                    e.add(Constantes.Color.VACIO);
                }
                a.add(e);
            }
            agenteJugador.setTablero(idP, a);
    }
}
