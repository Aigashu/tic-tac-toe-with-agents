/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.practica1.tareas;

import static es.uja.ssmmaa.curso1920.practica1.Constantes.MAX_PARTIDAS;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.TIME_OUT;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import java.util.Date;

/**
 *
 * @author nervi
 */
public class TareaRepitePropose extends TickerBehaviour {

    Tablero_Interface tab;

    public TareaRepitePropose(Agent a, long period) {
        super(a, period);
        tab = (Tablero_Interface) a;
    }

    @Override
    protected void onTick() {
        if (tab.getTablero_gui().size() < MAX_PARTIDAS) {
            ACLMessage propose = new ACLMessage(ACLMessage.PROPOSE);
            propose.setProtocol(FIPANames.InteractionProtocol.FIPA_PROPOSE);
            propose.setSender(myAgent.getAID());
            for (Object agentes : tab.getListaAgentes()) {
                propose.addReceiver((AID) agentes);
            }
            propose.setReplyByDate(new Date(System.currentTimeMillis() + TIME_OUT));
            myAgent.addBehaviour(new TareaProposeIniciaPartida((Agent) tab, propose));
        }

    }

}
