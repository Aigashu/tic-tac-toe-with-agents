/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.practica1.tareas;


import static es.uja.ssmmaa.curso1920.practica1.Constantes.ESPERA;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.TIME_OUT;
import es.uja.ssmmaa.curso1920.practica1.util.GsonUtil;
import es.uja.ssmmaa.curso1920.practica1.util.Movimiento;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;

/**
 *
 * @author nervi
 */
public class TareaRequestIniciaMovimiento extends AchieveREInitiator {

    private Tablero_Interface<Movimiento> agenteTablero;
    private Movimiento msgInfo;
    private final GsonUtil gsonUtil;
    private Boolean haGanado;
    private Boolean empate ;

    public TareaRequestIniciaMovimiento(Agent a, ACLMessage msg) {
        super(a, msg);
        gsonUtil = new GsonUtil<>();
        msgInfo = null;
        agenteTablero = (Tablero_Interface) a;
        haGanado = false;
        empate = false;
    }

    @Override
    protected void handleAllResponses(Vector responses) {
        Iterator it = responses.iterator();

        while (it.hasNext()) {
            //MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
            ACLMessage msg = (ACLMessage) it.next();
            String infoResponse;
            try {
                String mensajeString = (String) gsonUtil.decode(msg.getContent(), String.class);

                switch (msg.getPerformative()) {
                    case ACLMessage.NOT_UNDERSTOOD:
                        infoResponse = "El jugador " + mensajeString.split(" ")[1] + " NO ENTIENDE la solicitud de  jugar la partida: " + mensajeString.split(" ")[0] +"\n"  + "Se cerrará la partida por tanto\n"; 
                        agenteTablero.getInfo().addInfoPartida(infoResponse);
                        break;
                    case ACLMessage.REFUSE:
                        // Ficha Vacio para mostrar que no se mete
                        infoResponse = "El jugador " + mensajeString.split(" ")[0] + " Deniega jugar la partida\n" + "Se procedera a terminarla por tanto";
                        agenteTablero.getInfo().addInfoPartida(infoResponse);
                        break;
                    case ACLMessage.AGREE:
                        /// SE DEJA EL MENSAJE TAL CUAL VIENE
                        infoResponse = "El jugador " + mensajeString.split(" ")[1] + " Acepta jugar la partida: " + mensajeString.split(" ")[0] +"\n";
                        agenteTablero.getInfo().addInfoPartida(infoResponse);
                        break;
                }
            } catch (Exception e) {
                System.out.println("EXCEPCION. Se procedera a terminar handle por tanto");
            }

        }
    }

    /**
     * Resultado para la operación solicitada de los AgenteOperación que
     * aceptaron la realización.
     *
     * @param resultNotifications Mensaje ACL con el resultado de la operación.
     */
    @Override
    protected void handleAllResultNotifications(Vector resultNotifications) {
        Iterator it = resultNotifications.iterator();

        while (it.hasNext()) {
            ACLMessage msg = (ACLMessage) it.next();
            String infoNotification;
            try {
                msgInfo = (Movimiento) gsonUtil.decode(msg.getContent(), Movimiento.class);
                int idP = Integer.parseInt(msgInfo.getIdPartida());
                switch (msg.getPerformative()) {
                    case ACLMessage.FAILURE:
                        infoNotification = "Se deniega el movimiento por parte del jugador : " + msgInfo.getJugador().getNickName() + " para la partida: " + msgInfo.getIdPartida() + "\n" + "Se procede a cerrar la partida";
                        agenteTablero.getInfo().addInfoPartida(infoNotification);
                        break;
                    case ACLMessage.INFORM:
                        Thread.sleep(1200);
                        infoNotification  = "Se ha recibido el movimiento por parte del jugador : " + msgInfo.getJugador().getNickName() + " para la partida: " + msgInfo.getIdPartida() + "\n";
                        agenteTablero.getInfo().addInfoPartida(infoNotification);
                        agenteTablero.setUltMov(Integer.parseInt(msgInfo.getIdPartida()), msgInfo);
                        agenteTablero.setMensaje(msg.getSender(), msgInfo);
                        
                        haGanado = agenteTablero.compruebaVictoria(msg, msgInfo, empate);
                        if (!haGanado && !empate) {
                            // Envia request movimiento
                            String pareja;
                            if (agenteTablero.getTablero_gui().get(Integer.parseInt(msgInfo.getIdPartida())).getListaJugadores()[0].equals(msg.getSender().getLocalName())) {
                                pareja = agenteTablero.getTablero_gui().get(Integer.parseInt(msgInfo.getIdPartida())).getListaJugadores()[1];
                            } else {
                                pareja = agenteTablero.getTablero_gui().get(Integer.parseInt(msgInfo.getIdPartida())).getListaJugadores()[0];
                            }
                            ACLMessage men = new ACLMessage(ACLMessage.REQUEST);
                            men.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
                            men.setReplyByDate(new Date(System.currentTimeMillis() + TIME_OUT));
                            for (Object operacion : agenteTablero.getListaJugadores()) {
                                AID op = (AID) operacion;
                                if (op.getLocalName().equals(pareja)) {
                                    men.addReceiver((AID) operacion);
                                }
                            }
                            //el contenido del mensaje ahora será el movimientoanterior 

                            men.setContent(gsonUtil.encode(msgInfo, Movimiento.class));
                            myAgent.addBehaviour(new TareaRequestIniciaMovimiento((Agent) agenteTablero, men));
                        }
                        
                        break;
                }
            } catch (Exception e) {
                System.out.println("Se ha producido una excepcion, no se puede tratar el movimiento. SE PROCEDERA A CERRAR PARTIDA");
            }

            if (haGanado || empate) {
                break;
            }

        }
    }

    /**
     * Si algún mensaje llega fuera de la secuencia esperada para el protocolo
     * Request.
     *
     * @param msg Mensaje ACL que ha llegado fuera de la secuencia.
     */
    @Override
    protected void handleOutOfSequence(ACLMessage msg) {

        System.out.println("Mensaje fuera de Secuencia");
    }

}
