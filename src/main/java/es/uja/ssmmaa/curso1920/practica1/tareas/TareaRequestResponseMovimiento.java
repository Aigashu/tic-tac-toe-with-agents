/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.practica1.tareas;

import es.uja.ssmmaa.curso1920.practica1.Constantes;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.COLORES;
import es.uja.ssmmaa.curso1920.practica1.Constantes.Color;
import static es.uja.ssmmaa.curso1920.practica1.Constantes.NUM_ENRAYA;
import es.uja.ssmmaa.curso1920.practica1.util.GsonUtil;
import es.uja.ssmmaa.curso1920.practica1.util.Jugador;
import es.uja.ssmmaa.curso1920.practica1.util.Movimiento;
import es.uja.ssmmaa.curso1920.practica1.util.Posicion;
import jade.core.Agent;
import jade.domain.FIPAAgentManagement.FailureException;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.AchieveREResponder;
import java.util.ArrayList;

/**
 *
 * @author nervi
 */
public class TareaRequestResponseMovimiento extends AchieveREResponder {

    private GsonUtil gsonUtil;
    private Movimiento msgInfo;
    private Partida agenteJugador;
    private int idPartida;
    private Color fic;
    Jugador jug;
    Boolean centroPuesto;

    public TareaRequestResponseMovimiento(Agent a, MessageTemplate mt) {
        super(a, mt);
        gsonUtil = new GsonUtil<>();
        msgInfo = null;
        agenteJugador = (Partida) a;
        idPartida = 5000;
        fic = Color.VACIO;
        jug = null;
        centroPuesto = false;
    }

    /**
     * Se toma la decisión para realizar la operación solicitada. Utilizamos un
     * % ACEPTAR para decidir si se acepta o no la realización. Simula que el
     * agente puede tomar la decisión para realizar o no la acción.
     *
     * @param request Mensaje ACL con el Punto2D.
     * @return Mensaje ACL con el resultado aceptando.
     * @throws RefuseException Mensaje ACL con el resultado del rechazo.
     * @throws NotUnderstoodException Mensaje ACL si no hay contenido en el
     * mensaje.
     */
    @Override
    protected ACLMessage handleRequest(ACLMessage request) throws RefuseException, NotUnderstoodException {

        if (request.getContent() == null) {
            throw new NotUnderstoodException(gsonUtil.encode(request.getContent().split(" ")[0] + " " + getAgent().getLocalName(), String.class));
        }

        ACLMessage agree = request.createReply();
        agree.setPerformative(ACLMessage.AGREE);

        try { //Si no funciona el request se deniega
            msgInfo = (Movimiento) gsonUtil.decode(request.getContent(), Movimiento.class);
            if (msgInfo.getPosicion().getX() == -1) {  // Te han enviado un pedido de inicio. Vas a ser el primer agente que mande un movimiento, el resto se movera a partir del tuyo

                idPartida = Integer.parseInt(msgInfo.getIdPartida());
                jug = new Jugador(myAgent.getLocalName());
                fic = giveFicha(msgInfo.getFicha().toString(), 0);

                agenteJugador.addIdPartida(idPartida);
                agenteJugador.setColor(idPartida, fic);
                agenteJugador.addAgentTablero(idPartida, request.getSender());
                agree.setContent(gsonUtil.encode(idPartida + " " + myAgent.getLocalName() + " " + agenteJugador.getColor(idPartida), String.class));
            } else { // Te han enviado un movimiento

                idPartida = Integer.parseInt(msgInfo.getIdPartida());
                jug = new Jugador(myAgent.getLocalName());
                fic = giveFicha(msgInfo.getFicha().toString(), 1);

                agenteJugador.setColor(idPartida, fic);
                agenteJugador.setPosTablero(idPartida, msgInfo.getFicha(), msgInfo.getPosicion());
                agenteJugador.addIdPartida(idPartida);
                agenteJugador.addAgentTablero(idPartida, request.getSender());
                agree.setContent(gsonUtil.encode(idPartida + " " + myAgent.getLocalName() + " " + fic, String.class));
            }

        } catch(Exception e) {
            msgInfo = new Movimiento("");
            msgInfo.setJugador(new Jugador(getAgent().getLocalName()));
            msgInfo.setFicha(COLORES[2]);
            throw new RefuseException("" + " " + gsonUtil.encode(getAgent().getLocalName() + " ", String.class));
        }
        return agree;
    }

    /**
     * Se realiza una operación aleatoria de entre Operacion sobre el Punto2D y
     * se envía el resultado de la operación como resultado.
     *
     * @param request Mensaje ACL con el Punto2D.
     * @param response Mensaje ACL con la respuesta enviada en el método
     * anterior.
     * @return Mensaje ACL con el resultado de la operación.
     * @throws FailureException Mensaje ACL con el motivo del fallo de la
     * operación solicitada.
     */
    @Override
    protected ACLMessage prepareResultNotification(ACLMessage request, ACLMessage response)
            throws FailureException {
        ACLMessage inform = request.createReply();
        inform.setPerformative(ACLMessage.INFORM);

        GsonUtil<Movimiento> gsonPunto2D = new GsonUtil();
        try {

            msgInfo = new Movimiento(idPartida + "");
            Posicion p = SeleccionaPosicion(idPartida);
            msgInfo.setPosicion(p);

            msgInfo.setFicha(fic);
            msgInfo.setJugador(jug);
            agenteJugador.setPosTablero(idPartida, agenteJugador.getColor(idPartida), p);
            inform.setContent(gsonUtil.encode(msgInfo, Movimiento.class));
        } catch (Exception e) {
            // se le devuelve el movimiento tal cual lo ha enviado  porque no se puede operar con el
            System.out.println("EXCEPCION: " + e);
            msgInfo = new Movimiento(idPartida + "");
            msgInfo.setJugador(new Jugador(myAgent.getLocalName()));
            throw new FailureException(gsonUtil.encode(msgInfo, Movimiento.class));
        }

        return inform;
    }


    private Posicion SeleccionaPosicion(int id) {
        //Hacer la IA de la partida
        int x, y, i;
        x = y = i = 0;
        boolean posicionConseguida = false;
        if(!centroPuesto && agenteJugador.getTablero(id).get(1).get(1) == Color.VACIO ){
            x= 1;
            y= 1;
            return new Posicion(x, y);
        }
        while (i < agenteJugador.getTablero(id).size() && !posicionConseguida) {
            for (int j = 0; j < agenteJugador.getTablero(id).get(i).size(); j++) {
                if (agenteJugador.getTablero(id).get(i).get(j) == Color.VACIO) {
                    x = i;
                    y = j;
                    posicionConseguida = true;
                    break;
                }

            }
            i++;
        }
        return new Posicion(x, y);
    }

    private Color giveFicha(String f, int peticion) {
        if (peticion == 1) { // Peticion = 1, te han mandado la ficha del otro jugador, asi que tu coges la contraria
            switch (f) {
                case "ROJO":
                    return Constantes.Color.AZUL;
                default:
                    return Constantes.Color.ROJO;

            }
        } else { //Peticion = 0, quiere decir que el agenteTablero te ha asignado ficha al ser tu el primer jugador
            switch (f) {
                case "ROJO":
                    return Constantes.Color.ROJO;
                default:
                    return Constantes.Color.AZUL;

            }
        }

    }
    
}
