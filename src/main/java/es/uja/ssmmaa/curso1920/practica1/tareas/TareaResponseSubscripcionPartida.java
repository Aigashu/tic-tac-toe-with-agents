/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.practica1.tareas;

import es.uja.ssmmaa.curso1920.practica1.util.GsonUtil;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.FIPAAgentManagement.FailureException;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.SubscriptionResponder;

/**
 *
 * @author nervi
 */
public class TareaResponseSubscripcionPartida extends SubscriptionResponder {

    private Subscription suscripcionJugador;
    private Tablero_Interface agenteTablero;
    private GsonUtil gsonUtil;

    public TareaResponseSubscripcionPartida(Agent a, MessageTemplate mt, SubscriptionManager sm) {
        super(a, mt,sm);
        agenteTablero = (Tablero_Interface) a ;
        gsonUtil = new GsonUtil();
        
    }

    /**
     * Se cancelará la subscripión activa de un agente atendiendo a su soliciud.
     * Si hay algún problema en el objeto Subscription asociado se comunicará
     * que ha fallado la cancelación.
     *
     * @param cancel Mensaje ACL de cancelación.
     * @return Mensaje ACL informando sobre la cancelación.
     * @throws FailureException Mensaje ACL si se ha producido algún problema en
     * la cancelación.
     */

    @Override
    protected ACLMessage handleCancel(ACLMessage cancel) throws FailureException {

        // Eliminamos la suscripción del agente jugador
        String error;
        try {
            suscripcionJugador = agenteTablero.getSubscripcion(cancel.getSender());
            mySubscriptionManager.deregister(suscripcionJugador);
            error = "Cancelacion de Agente: " + cancel.getSender() + "Correctamente realizada\n";
        } catch (Exception e) {
            error = myAgent.getLocalName() + " : \n\t" + "Error al cancelar la subscripción para " + cancel.getSender();
            throw new FailureException(gsonUtil.encode(error, String.class));
        }

        // Mensaje de cancelación
        ACLMessage cancela = cancel.createReply();
        cancela.setPerformative(ACLMessage.INFORM);
        cancela.setSender(myAgent.getAID());
        cancela.setContent(gsonUtil.encode(error, String.class));
        return cancela;
    }

    /**
     * Se aceptará la subscripció si no se produce ningún problema en la
     * generación del objeto Subscription asociado al agente que la solicita.
     *
     * @param subscription Mensaje ACL con la solicitud de subscripción.
     * @return Mensaje ACL con la aceptación de la subscripción.
     * @throws NotUnderstoodException No se trata por no analizar el contenido
     * del mensaje subscription
     * @throws RefuseException Mensaje ACL con el rechazo si no se ha podico
     * completar la subscripción.
     */
    @Override
    protected ACLMessage handleSubscription(ACLMessage subscription) throws NotUnderstoodException,
            RefuseException {

        String subscrito;
        try {
            // Registra la suscripción si no hay una previa
            if (!agenteTablero.haySubscripcion(subscription.getSender())) {
                suscripcionJugador = createSubscription(subscription);
                mySubscriptionManager.register(suscripcionJugador);
            }
            
        } catch (Exception e) {
            System.out.println("EXCEPCION SUBSCRIBE:" + e);
            subscrito = "Error al registrar la subscripción en " + myAgent.getLocalName();
            throw new RefuseException(gsonUtil.encode(subscrito, String.class));
        }

        // Responde afirmativamente a la suscripción
        ACLMessage agree = subscription.createReply();
        subscrito = "Subscripción creada en " + myAgent.getLocalName();
        agree.setPerformative(ACLMessage.AGREE);
        agree.setContent(gsonUtil.encode(subscrito, String.class));
        return agree;
    }
}
