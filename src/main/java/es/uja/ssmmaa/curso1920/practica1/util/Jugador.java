/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.practica1.util;

import jade.core.AID;

/**
 *
 * @author pedroj
 */
public class Jugador {
    private final String nickName;

    public Jugador(String nickName) {
        this.nickName = nickName;
    }

    public String getNickName() {
        return nickName;
    }


    @Override
    public String toString() {
        return "Jugador{" + "nickname = " + nickName + "}";
    }
}
