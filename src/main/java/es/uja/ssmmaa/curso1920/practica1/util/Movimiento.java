/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.practica1.util;

import es.uja.ssmmaa.curso1920.practica1.Constantes.Color;

/**
 *
 * @author pedroj
 */
public class Movimiento {
    private final String idPartida;
    private Jugador jugador;
    private Color ficha;
    private Posicion posicion;

    public Movimiento(String idPartida) {
        this.idPartida = idPartida;
        this.jugador = null;
        this.ficha = null;
        this.posicion = null;
    }

    public String getIdPartida() {
        return idPartida;
    }
    
    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public Color getFicha() {
        return ficha;
    }

    public void setFicha(Color ficha) {
        this.ficha = ficha;
    }

    public Posicion getPosicion() {
        return posicion;
    }

    public void setPosicion(Posicion posicion) {
        this.posicion = posicion;
    }

    @Override
    public String toString() {
        return "Movimiento{" + "idPartida:" + idPartida + ":, jugador:" + 
                jugador + ":, ficha:" + ficha + ":, posicion:" + posicion + ":}";
    }   
}
