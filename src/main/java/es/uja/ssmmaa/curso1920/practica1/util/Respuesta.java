/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.practica1.util;

import es.uja.ssmmaa.curso1920.practica1.Constantes.Color;

/**
 *
 * @author pedroj
 */
public class Respuesta {
    private String nombreAgente;
    private Movimiento mov;

    public Respuesta(String nombreAgente) {
        this.nombreAgente = nombreAgente;
    }

    public String getNombreAgente() {
        return nombreAgente;
    }

    public void setNombreAgente(String nombreAgente) {
        this.nombreAgente = nombreAgente;
    }

    public Movimiento getMov() {
        return mov;
    }

    public void setMov(Movimiento mov) {
        this.mov = mov;
    }
    @Override
    public String toString() {
        Jugador j = mov.getJugador();
        Color c = mov.getFicha();
        int x = mov.getPosicion().getX();
        int y = mov.getPosicion().getY() ;
        return "Respuesta{" + "nombreAgente=" + nombreAgente + ", IDPartida = " + mov.getIdPartida() + " , Jugador = " + j  + ", Color = " + c
                 + "en la posicion: " + " ( "+ x + " , "+ y + ") }";
    }
    
    
}
